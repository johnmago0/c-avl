/*  This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AVL_H_
#define AVL_H_

#include <stdbool.h>
#include <stdint.h>

typedef uint32_t keytype;

struct node_t {
  keytype key;
  void *data;
  int height;
  struct node_t *parent;
  struct node_t *left;
  struct node_t *right;
};

/* NOTE: All deallocation of the data inserted in the tree is responsability of
 * it, you should not deallocate data inside the tree without using one of */
/* it functions for it. */
struct tree_t {
  struct node_t *head;
  void (*del_function)(void *);
  int size;
};

/* Create a tree and initializes it's values.
 *
 * @del:
 *  Function pointer to delete the data stored in the node.
 *  If the function value is NULL, a simple free() calls will be used. */
struct tree_t *tree_create(void (*del)(void *));

/* Deallocate all the values inside the tree(including the tree) and
 * set it to NULL.
 * NOTE: This function deallocate all the data stored on the nodes. */
void tree_destroy(struct tree_t **tree);

/* Inserts a value on the tree. */
bool tree_insert(struct tree_t *tree, keytype key, void *data);

/* Unlink the node from the tree and deallocate the value stored on it. */
bool tree_remove(struct tree_t *tree, keytype key);

/* Search the tree by the key. */
const void *tree_search(const struct tree_t *tree, keytype key);

/* Print utility for debugging.
 * This function is recursive, so watch out when using it. */
void tree_print(const struct tree_t *tree);

#endif // AVL_H_
