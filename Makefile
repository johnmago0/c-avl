test:
	gcc -g3 -Wall -pedantic tree.c test_tree.c -o test.a

valgrind: test
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=valgrind-out.txt ./test.a
