/*  This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "tree.h"

typedef struct node_t node_t;
typedef struct tree_t tree_t;

#include <stdio.h>
#include <stdlib.h>

#define max(a, b) ((a) > (b) ? (a) : (b))

#define node_height(node) ((node) == NULL ? 0 : (node)->height)

#define load_balance(node)                                                     \
  ((node) == NULL ? 0 : (node_height(node->left) - node_height(node->right)))

#define swap(a, b, type)                                                       \
  {                                                                            \
    type tmp = (a);                                                            \
    (a) = (b);                                                                 \
    (b) = tmp;                                                                 \
  }

/*============================================================================*/
/*-----                             NODE                                 -----*/
/*============================================================================*/

struct node_t *tree_node_create(keytype key, void *data);

/* Insert a node as a left or right child of the given node.
 * Returns true if the insertion was successful and false otherwise. */
static bool node_insert_child(node_t *node, keytype key, void *data) {
  if (node->key > key && node->left == NULL) {
    node->left = tree_node_create(key, data);
    node->left->parent = node;
    return true;
  }

  if (node->key < key && node->right == NULL) {
    node->right = tree_node_create(key, data);
    node->right->parent = node;
    return true;
  }

  return false;
}

/* Update the height of the node.
 * The height of a node is given by the maximum height of its children
 * plus 1.*/
static void node_update_height(node_t *node) {
  if (node == NULL)
    return;

  node->height = 1 + max(node_height(node->left), node_height(node->right));
}

/* Find the successor of the node.
 * The successor is determined by the lowest value in the right
 * subtree of the node. */
static node_t *node_successor_find(node_t *node) {
  node_t *iter = node->right;

  while (iter->left != NULL)
    iter = iter->left;

  return iter;
}

/* Finds the successor of the node and swaps its data with the given
 * node's data. */
static void node_successor_swap(node_t *node) {
  node_t *succ = node_successor_find(node);
  node->key = succ->key;
  swap(node->data, succ->data, void *);
}

/* Update the parent of the node. */
static void node_parent_update(node_t *node, node_t *parent) {
  if (node == NULL)
    return;

  node->parent = parent;
}

/* Unlinks the given node from its parent.
 * Returns the parent node.*/
static node_t *node_unlink(node_t *node) {
  node_t *nodeParent = node->parent;

  if (nodeParent != NULL) {
    if (nodeParent->left == node)
      nodeParent->left = NULL;
    else
      nodeParent->right = NULL;
  }

  return nodeParent;
}

/* Delete the node. */
static void node_delete(node_t *node, void (*del_function)(void *)) {
  if (del_function == NULL) {
    free(node->data);
    node->data = NULL;
  } else {
    del_function(node->data);
    node->data = NULL;
  }
  free(node);
  node = NULL;
}

/* Swap the contents of one node to the other, except for the parent. */
static void node_copy(node_t *dst, node_t *src) {
  swap(dst->left, src->left, node_t *);
  swap(dst->right, src->right, node_t *);
  swap(dst->key, src->key, keytype);
  swap(dst->data, src->data, void *);
}

/* Remove a node from the tree, handling cases where the node has either
 * one child or none. In the case of two children, the successor's data is
 * swapped with the current node's data. */
static void node_remove(node_t **node, void (*del_function)(void *)) {
  node_t *deadNode = (*node)->left != NULL ? (*node)->left : (*node)->right;
  if (deadNode == NULL) {
    deadNode = *node;
    *node = node_unlink(deadNode);
    node_delete(deadNode, del_function);
    return;
  }

  node_copy(*node, deadNode);
  node_delete(deadNode, del_function);
}

/* Create a node and initializes it's values */
node_t *tree_node_create(keytype key, void *data) {
  node_t *node = (node_t *)malloc(sizeof(node_t));
  node->key = key;
  node->data = data;
  node->height = 1;
  node->parent = NULL;
  node->left = NULL;
  node->right = NULL;
  return node;
}

/*============================================================================*/
/*-----                             ROTATION -----*/
/*============================================================================*/

/* Perform a rotation of the node to the left side.
 *
 *        root                       pivot
 *        /   \                      /   \
 *    subroot pivot      =>        root   X
 *             /  \                /  \
 *            /    \              /    \
 *        subpivot  X         subroot subpivot
 */
static void rotate_left(node_t *node) {
  node_t *root = node;
  node_t *pivot = root->right;
  node_t *subRoot = root->left;
  node_t *subPivot = pivot->left;

  // To perform the rotation between the pivot and the root, only the key and
  // the data are swapped. In this way it's not necessary to change the parent's
  // node of the root.
  swap(root->key, pivot->key, keytype);
  swap(root->data, pivot->data, void *);

  root->right = pivot->right;

  pivot->right = subPivot;
  pivot->left = subRoot;

  root->left = pivot;

  node_parent_update(subRoot, pivot);
  node_parent_update(root->right, root);

  node_update_height(pivot);
  node_update_height(root);
}

/* Perform a rotation of the node to the right side.
 *
 *       root                 pivot
 *       /   \                /   \
 *    pivot  subtroot  =>    X   root
 *     / \                        / \
 *    /   \                      /   \
 *   X  subpivot            subpivot subtroot
 */
static void rotate_right(node_t *node) {
  node_t *root = node;
  node_t *pivot = root->left;
  node_t *subRoot = root->right;
  node_t *subPivot = pivot->right;

  // To perform the rotation between the pivot and the root, only the key and
  // the data are swapped. In this way it's not necessary to change the parent's
  // node of the root.
  swap(root->key, pivot->key, keytype);
  swap(root->data, pivot->data, void *);

  root->left = pivot->left;

  pivot->left = subPivot;
  pivot->right = subRoot;

  root->right = pivot;

  node_parent_update(subRoot, pivot);
  node_parent_update(root->left, root);

  node_update_height(pivot);
  node_update_height(root);
}

/*============================================================================*/
/*-----                            TREE                                  -----*/
/*============================================================================*/

/* Reduce the size of the tree. If the size goes to 0, set the head to NULL */
#define tree_size_subtract(tree)                                               \
  {                                                                            \
    (tree)->size--;                                                            \
    if ((tree)->size == 0)                                                     \
      (tree)->head = NULL;                                                     \
  }

#define tree_size_add(tree) (tree)->size++

/* Handles the growth of the tree after a insertion. There are four cases that
 * make the need to rebalance the tree:
 *
 * 1. Left-Left Case:
 *        z                                      y
 *       / \                                   /   \
 *      y   T4      Right Rotate (z)          x      z
 *     / \          - - - - - - - - ->      /  \    /  \
 *    x   T3                               T1  T2  T3  T4
 *   / \
 * T1   T2
 *
 * 2. Left-Right Case:
 *     z                               z                           x
 *    / \                            /   \                        /  \
 *   y   T4  Left Rotate (y)        x    T4  Right Rotate(z)    y      z
 *  / \      - - - - - - - - ->    /  \      - - - - - - - ->  / \    / \
 * T1  x                          y    T3                     T1 T2  T3  T4
 *    / \                        / \
 *  T2   T3                    T1   T2
 *
 * 3. Right-Right Case:
 *    z                               y
 *  /  \                            /   \
 * T1   y     Left Rotate(z)      z      x
 *    /  \   - - - - - - - ->    / \    / \
 *   T2   x                     T1  T2 T3  T4
 *       / \
 *     T3  T4
 *
 * 4. Right-Left Case:
 *    z                            z                            x
 *   / \                          / \                          /  \
 * T1   y   Right Rotate (y)    T1   x      Left Rotate(z)   z      y
 *     / \  - - - - - - - - ->     /  \   - - - - - - - ->  / \    / \
 *    x   T4                      T2   y                  T1  T2  T3  T4
 *   / \                              /  \
 * T2   T3                           T3   T4
 *
 */
static void tree_rebalance_insert(node_t *node, keytype key) {
  if (node == NULL)
    return;

  do {

    node_update_height(node);
    int balance = load_balance(node);

    // 1. Left-Left case
    if (balance > 1 && node->left->key > key) {
      rotate_right(node);
      break;
    }

    // 2. Left-Right case
    if (balance > 1 && node->left->key < key) {
      rotate_left(node->left);
      node_parent_update(node->left, node);
      rotate_right(node);
      break;
    }

    // 3. Right-Right case
    if (balance < -1 && node->right->key < key) {
      rotate_left(node);
      break;
    }

    // 4. Right-Left case
    if (balance < -1 && node->right->key > key) {
      rotate_right(node->right);
      node_parent_update(node->right, node);
      rotate_left(node);
      break;
    }

    node = node->parent;
  } while (node != NULL);
}

/* Handles the growth of the tree after a removal. There are four cases that
 * make the need to rebalance the tree. Unlike the insertion more than one
 * rotation may be needed to rebalance the tree:
 *
 * 1. Left-Left Case:
 *        z                                      y
 *       / \                                   /   \
 *      y   T4      Right Rotate (z)          x      z
 *     / \          - - - - - - - - ->      /  \    /  \
 *    x   T3                               T1  T2  T3  T4
 *   / \
 * T1   T2
 *
 * 2. Left-Right Case:
 *     z                               z                           x
 *    / \                            /   \                        /  \
 *   y   T4  Left Rotate (y)        x    T4  Right Rotate(z)    y      z
 *  / \      - - - - - - - - ->    /  \      - - - - - - - ->  / \    / \
 * T1  x                          y    T3                     T1 T2  T3  T4
 *    / \                        / \
 *  T2   T3                    T1   T2
 *
 * 3. Right-Right Case:
 *    z                               y
 *  /  \                            /   \
 * T1   y     Left Rotate(z)      z      x
 *    /  \   - - - - - - - ->    / \    / \
 *   T2   x                     T1  T2 T3  T4
 *       / \
 *     T3  T4
 *
 * 4. Right-Left Case:
 *    z                            z                            x
 *   / \                          / \                          /  \
 * T1   y   Right Rotate (y)    T1   x      Left Rotate(z)   z      y
 *     / \  - - - - - - - - ->     /  \   - - - - - - - ->  / \    / \
 *    x   T4                      T2   y                  T1  T2  T3  T4
 *   / \                              /  \
 * T2   T3                           T3   T4
 *
 */
static void tree_rebalance_remove(node_t *node) {
  do {
    node_update_height(node);
    int balance = load_balance(node);

    // 1. Left-Left Case:
    if (balance > 1 && load_balance(node->left) >= 0) {
      rotate_right(node);
      break;
    }

    // 2. Left-Right Case:
    if (balance > 1 && load_balance(node->left) < 0) {
      rotate_left(node->left);
      node_parent_update(node->left, node);
      rotate_right(node);
      break;
    }

    // 3. Right-Right Case:
    if (balance < -1 && load_balance(node->right) <= 0) {
      rotate_left(node);
      break;
    }

    // 4. Right-Left Case:
    if (balance < -1 && load_balance(node->right) > 0) {
      rotate_right(node->right);
      node_parent_update(node->right, node);
      rotate_left(node);
      break;
    }
    node = node->parent;
  } while (node != NULL);
}

/* Prints the tree in a preorder manner. */
static void avl_print(node_t *node, int id, const char *pos) {
  if (node == NULL)
    return;

  for (int i = 0; i < id; i++) {
    if (i == id - 1)
      printf(">");
    else
      printf("=");
  }

  printf("[%d]: %d %s\n", id, node->key, pos);
  avl_print(node->left, id + 1, "LEFT");
  avl_print(node->right, id + 1, "RIGHT");
}

/*============================================================================*/
/*-----                             AVL -----*/
/*============================================================================*/


tree_t *tree_create(void (*del)(void *)) {
  tree_t *tree = (tree_t *)malloc(sizeof(tree_t));
  tree->head = NULL;
  tree->del_function = del;
  tree->size = 0;
  return tree;
}

void tree_destroy(tree_t **tree) {
  node_t *iter = (*tree)->head;

  while (iter != NULL) {
    if (iter->left == NULL && iter->right == NULL) {
      node_remove(&iter, (*tree)->del_function);
      tree_size_subtract(*tree);
    } else if (iter->left != NULL)
      iter = iter->left;
    else if (iter->right != NULL)
      iter = iter->right;
  }

  free(*tree);
  *tree = NULL;
}

void *tree_search(const tree_t *tree, keytype key) {
  node_t *iter = tree->head;
  while (iter != NULL) {
    if (iter->key == key)
      return iter->data;

    if (iter->key < key)
      iter = iter->right;
    else
      iter = iter->left;
  }

  return NULL;
}

bool tree_insert(tree_t *tree, keytype key, void *data) {
  tree_size_add(tree);

  if (tree->head == NULL) {
    tree->head = tree_node_create(key, data);
    return true;
  }

  node_t *iter = tree->head;
  while (true) {
    if (iter->key == key)
      return false;

    if (node_insert_child(iter, key, data))
      break;

    if (iter->key > key)
      iter = iter->left;
    else
      iter = iter->right;
  }

  tree_rebalance_insert(iter, key);
  return true;
}

bool tree_remove(tree_t *tree, keytype key) {
  bool deleted = false;

  node_t *iter = tree->head;
  while (iter != NULL) {
    if (iter->key == key) {
      if (iter->left == NULL || iter->right == NULL) {
        node_remove(&iter, tree->del_function);
        tree_size_subtract(tree);
        deleted = true;
        break;
      } else {
        node_successor_swap(iter);
        iter = iter->right;
        key = iter->key;
        continue;
      }
    }

    if (iter->key > key)
      iter = iter->left;
    else
      iter = iter->right;
  }

  if (iter == NULL)
    return deleted;

  tree_rebalance_remove(iter);
  return deleted;
}

void tree_print(const tree_t *tree) { avl_print(tree->head, 0, "TOP"); }
