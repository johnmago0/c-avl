#include "tree.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define TEST_COUNT 4
#define LOOPCOUNT 10
#define PRINT 0

bool test_insertion() {
  int errs = 0;

  struct tree_t *tree = tree_create(NULL);
  for (int i = 0; i < LOOPCOUNT; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    if (!tree_insert(tree, i, x)) {
      errs++;
#ifdef PRINT
      printf("Insertion (%d) didn't work\n", i);
      tree_print(tree);
#endif
    }
  }

  tree_destroy(&tree);
  return (errs == 0);
}

bool test_removal() {
  int errs = 0;

  struct tree_t *tree = tree_create(NULL);
  for (int i = 0; i < LOOPCOUNT; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    tree_insert(tree, i, x);
  }

  for (int i = 0; i < LOOPCOUNT; i++) {
    if (!tree_remove(tree, i)) {
      errs++;
#ifdef PRINT
      printf("Remove (%d) didn't work\n", i);
      tree_print(tree);
#endif
    }
  }

  tree_destroy(&tree);
  return (errs == 0);
}

struct TestingStrc {
  int *x;
  char *str;
};

static struct TestingStrc* create_testing_strc() {
  struct TestingStrc *tsrc =
      (struct TestingStrc *)malloc(sizeof(struct TestingStrc));
  
  tsrc->x = (int*)malloc(sizeof(int));
  tsrc->str = (char*)malloc(sizeof(char));

  return tsrc;
}

static void remove_testing_strc(void *data) {
  struct TestingStrc *tsrc = (struct TestingStrc *)data;
  free(tsrc->x);
  free(tsrc->str);
  free(data);
}

bool test_removal_complex() {
  int errs = 0;

  struct tree_t *tree = tree_create(remove_testing_strc);
  for (int i = 0; i < LOOPCOUNT; i++) {
    struct TestingStrc *x = create_testing_strc();
    tree_insert(tree, i, x);
  }

  for (int i = 0; i < LOOPCOUNT; i++) {
    if (!tree_remove(tree, i)) {
      errs++;
#ifdef PRINT
      printf("Remove (%d) didn't work\n", i);
      tree_print(tree);
#endif
    }
  }

  tree_destroy(&tree);
  return (errs == 0);
}

bool test_searching() {
  int errs = 0;

  struct tree_t *tree = tree_create(NULL);
  for (int i = 0; i < LOOPCOUNT; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    tree_insert(tree, i, x);
  }

  struct node_t *node = NULL;
  for (int i = 0; i < LOOPCOUNT; i++) {
    node = tree_search(tree, i);
    if (node == NULL) {
      errs++;
#ifdef PRINT
      printf("Remove (%d) didn't work\n", i);
      tree_print(tree);
#endif
    }
  }

  tree_destroy(&tree);
  return (errs == 0);
}

bool test_removal_top() {
  struct tree_t *tree = tree_create(NULL);
  for (int i = 0; i < LOOPCOUNT; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    tree_insert(tree, i, x);
  }

  tree_remove(tree, 3);

  tree_destroy(&tree);
  return true;
}

int main() {
  uint32_t errs = TEST_COUNT;
  if (!test_insertion()) {
    printf("Insertion did not work\n");
    errs--;
  }

  if (!test_removal()) {
    printf("Removal did not work\n");
    errs--;
  }

  if (!test_removal_complex()) {
    printf("Complex data removal did not work\n");
    errs--;
  }

  if (!test_searching()) {
    printf("Searching did not work\n");
    errs--;
  }

  test_removal_top();

  printf("Test %d/%d passed\n", errs, TEST_COUNT);
}
